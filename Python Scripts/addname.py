# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 14:00:50 2020

@author: travo
"""


#Define main function
def main():
    #create an empty list
    name_list=[]
    #create a variable to control the loop
    again = 'y'
    #add names from the user
    while again == 'y':
        #Get a name from the user
        name = input('Enter a name: ')
        #append the name to the list
        name_list.append(name)
        #Display name
        print('Do you want to add another name?')
        again = input('y = yes, anything else = no: ')
        print()
    
    #Display the names that were entered
    print('Here are the names you entered')
    
    for name in name_list:
        print(name)
    
    #call the main function
    main()