# -*- coding: utf-8 -*-
"""
Created on Sat Jun  6 01:39:36 2020

@author: travo
"""


KILOMETERS_TO_MILES = 0.6214
def main():
    mykilometers = 0.0
    mykilometers = float(input("Enter the distance in kilometers: "))
    showMiles(mykilometers) # 10
    
def showMiles(distance):
    miles = 0.0 #variables
    miles = distance * KILOMETERS_TO_MILES
    print('The conversion of ', format(distance, '.2f'), 'kilometers')
    print(' to miles is' , format (miles, '.2f'), ' miles')


#Execute function
main()    