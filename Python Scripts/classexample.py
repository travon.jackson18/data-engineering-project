# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 23:39:10 2020

@author: travo
"""
import random

class Coin:
    #Initialize init method
    #sideup data attribute
    def __init__(self):
        self.sideup='Heads'
    def toss(self):
        if random.randint(0,1) == 0:
            self.sideup = 'Heads'
        else:
            self.sideup = 'Tails'
    
    def get_sideup(self):
        return self.sideup

            
        