# -*- coding: utf-8 -*-
"""
Created on Sat Jun  6 18:33:48 2020

@author: travo
"""


import math
#Define area function
def area(radius):
    return math.pi * radius **2
#def circumference
def circumference(radius):
    return 2*math.pi*radius