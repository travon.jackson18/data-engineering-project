# -*- coding: utf-8 -*-
"""
Created on Sat Jun  6 18:42:44 2020

@author: travo
"""


import circle
import rectangle

radius = float(input("Enter the circle's radius: "))
print('The area is ', circle.area(radius))
print ('The circumference is ', circle.circumference(radius))

width = float(input("Enter the rectangle's width: "))
length = float(input("Enter the rectangle's length: "))
print('The area is ', rectangle.area(width, length))
print('The perimeter is ', rectangle.perimeter(width, length))