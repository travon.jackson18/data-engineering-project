# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 00:09:18 2020

@author: travo
"""
import Coin

def main():
    #Create object of coin class
    my_coin = Coin()
    #Display the side of the coin that is facing up
    print('This side is up:', my_coin.get_sideup())
    my_coin.toss()
    print('This side is up:', my_coin.get_sideup())
    
main()