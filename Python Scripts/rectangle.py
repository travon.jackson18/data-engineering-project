# -*- coding: utf-8 -*-
"""
Created on Sat Jun  6 18:38:39 2020

@author: travo
"""


#area functions --> width and length
#returns the area
def area(width, length):
    return width*length

def perimeter(width, length):
    return 2 * (width + length)