# Constant
BASE_HOURS = 40

#Get the hours worked
hours = float(input("Enter the number of hours worked: "))
pay_rate = float(input("Enter the hourly pay rate: "))

#Calculate and display the gross pay
if hours > BASE_HOURS:
    #Calculate the gross pay with overtime
    overtime_hours = hours - BASE_HOURS
    #Calculate the amount of overtime pay
    overtime_pay = overtime_hours *pay_rate *1.5
    #Calculate the gross pay
    gross_pay = BASE_HOURS * pay_rate +overtime_pay
else:
    gross_pay = hours *pay_rate
    
#Display the gross pay

print ('The gross pay is $', format(gross_pay, '.2f'))