# -*- coding: utf-8 -*-
"""
Created on Sat Jun  6 01:54:23 2020

@author: travo
"""

import math

def main():
    #Get a number
    number = float(input('Enter a number: '))
    #Get the square root of the number
    square_root = math.sqrt(number)
    print('The square root of ', 'is ', square_root)
main()
